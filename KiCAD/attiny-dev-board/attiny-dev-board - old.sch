EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 3
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L MCU_Microchip_ATtiny:ATtiny85-20PU U?
U 1 1 5EFDBE3C
P 2005 2400
F 0 "U?" H 2235 1830 50  0000 R CNN
F 1 "ATtiny85-20PU" H 2635 2970 50  0000 R CNN
F 2 "Package_DIP:DIP-8_W7.62mm" H 2005 2400 50  0001 C CIN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/atmel-2586-avr-8-bit-microcontroller-attiny25-attiny45-attiny85_datasheet.pdf" H 2005 2400 50  0001 C CNN
	1    2005 2400
	1    0    0    -1  
$EndComp
$Sheet
S 9805 4320 510  440 
U 5EFE6AE5
F0 "USB_Serial" 50
F1 "USB_Serial.sch" 50
F2 "TXD" I L 9805 4500 50 
F3 "RXD" I L 9805 4600 50 
$EndSheet
$Sheet
S 9755 5190 660  390 
U 5F01C70C
F0 "Step_Down" 50
F1 "Step_Down.sch" 50
F2 "Vin" I L 9755 5330 50 
F3 "Vout_5V" O R 10415 5340 50 
F4 "Enable" I L 9755 5460 50 
$EndSheet
$Comp
L MCU_Microchip_ATmega:ATmega328P-AU U?
U 1 1 5EFE7EAC
P 7470 2820
F 0 "U?" H 7810 4280 50  0000 C CNN
F 1 "ATmega328P-AU" H 7830 1330 50  0000 C CNN
F 2 "Package_QFP:TQFP-32_7x7mm_P0.8mm" H 7470 2820 50  0001 C CIN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/ATmega328_P%20AVR%20MCU%20with%20picoPower%20Technology%20Data%20Sheet%2040001984A.pdf" H 7470 2820 50  0001 C CNN
	1    7470 2820
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 5F05E8A8
P 2450 5100
F 0 "R?" V 2345 5025 50  0000 C CNN
F 1 "1k" V 2345 5170 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 2380 5100 50  0001 C CNN
F 3 "~" H 2450 5100 50  0001 C CNN
	1    2450 5100
	0    1    1    0   
$EndComp
$Comp
L 74xGxx:74LVC1G3157 U?
U 1 1 5F0623ED
P 2000 5200
F 0 "U?" H 1825 5380 50  0000 C CNN
F 1 "74LVC1G3157" H 1975 5475 50  0001 C CNN
F 2 "Package_TO_SOT_SMD:SOT-363_SC-70-6_Handsoldering" H 2000 5200 50  0001 C CNN
F 3 "http://www.ti.com/lit/sg/scyt129e/scyt129e.pdf" H 2000 5200 50  0001 C CNN
	1    2000 5200
	1    0    0    -1  
$EndComp
Text Label 2705 2100 0    50   ~ 0
uC_PB0_MOSI_TX
Text Label 2705 2200 0    50   ~ 0
uC_PB1_MISO_RX
Text Label 2705 2300 0    50   ~ 0
uC_PB2_SCK_A1
Text Label 2705 2400 0    50   ~ 0
uC_PB3_XTAL1_A3
Text Label 2705 2500 0    50   ~ 0
uC_PB4_XTAL2_A2
Text Label 2705 2600 0    50   ~ 0
uC_PB5_RST_A0
Wire Wire Line
	2705 2100 2605 2100
Wire Wire Line
	2605 2200 2705 2200
Wire Wire Line
	2605 2300 2705 2300
Wire Wire Line
	2605 2400 2705 2400
Wire Wire Line
	2605 2500 2705 2500
Wire Wire Line
	2605 2600 2705 2600
$Comp
L 74xGxx:74LVC1G3157 U?
U 1 1 5F062B2B
P 1995 6165
F 0 "U?" H 1825 6335 50  0000 C CNN
F 1 "74LVC1G3157" H 1970 6440 50  0001 C CNN
F 2 "Package_TO_SOT_SMD:SOT-363_SC-70-6_Handsoldering" H 1995 6165 50  0001 C CNN
F 3 "http://www.ti.com/lit/sg/scyt129e/scyt129e.pdf" H 1995 6165 50  0001 C CNN
	1    1995 6165
	1    0    0    -1  
$EndComp
$Comp
L 74xGxx:74LVC1G3157 U?
U 1 1 5F065720
P 2000 7115
F 0 "U?" H 1825 7290 50  0000 C CNN
F 1 "74LVC1G3157" H 1975 7390 50  0001 C CNN
F 2 "Package_TO_SOT_SMD:SOT-363_SC-70-6_Handsoldering" H 2000 7115 50  0001 C CNN
F 3 "http://www.ti.com/lit/sg/scyt129e/scyt129e.pdf" H 2000 7115 50  0001 C CNN
	1    2000 7115
	1    0    0    -1  
$EndComp
Text Label 1700 7115 2    50   ~ 0
uC_PB1_MISO_RX
Text Label 1695 6165 2    50   ~ 0
uC_PB2_SCK_A1
Wire Wire Line
	1800 7115 1700 7115
Wire Wire Line
	1795 6165 1695 6165
Wire Wire Line
	1700 5200 1800 5200
Text Label 1700 5200 2    50   ~ 0
uC_PB0_MOSI_TX
$Comp
L power:GND #PWR?
U 1 1 5F076D36
P 2000 5440
F 0 "#PWR?" H 2000 5190 50  0001 C CNN
F 1 "GND" H 2140 5440 50  0000 C CNN
F 2 "" H 2000 5440 50  0001 C CNN
F 3 "" H 2000 5440 50  0001 C CNN
	1    2000 5440
	1    0    0    -1  
$EndComp
Wire Wire Line
	2000 5440 2000 5400
$Comp
L power:GND #PWR?
U 1 1 5F078F4C
P 1995 6405
F 0 "#PWR?" H 1995 6155 50  0001 C CNN
F 1 "GND" H 2135 6405 50  0000 C CNN
F 2 "" H 1995 6405 50  0001 C CNN
F 3 "" H 1995 6405 50  0001 C CNN
	1    1995 6405
	1    0    0    -1  
$EndComp
Wire Wire Line
	1995 6405 1995 6365
$Comp
L power:GND #PWR?
U 1 1 5F079B22
P 2000 7355
F 0 "#PWR?" H 2000 7105 50  0001 C CNN
F 1 "GND" H 2140 7355 50  0000 C CNN
F 2 "" H 2000 7355 50  0001 C CNN
F 3 "" H 2000 7355 50  0001 C CNN
	1    2000 7355
	1    0    0    -1  
$EndComp
Wire Wire Line
	2000 7355 2000 7315
$Comp
L power:+5V #PWR?
U 1 1 5F07A853
P 2000 4920
F 0 "#PWR?" H 2000 4770 50  0001 C CNN
F 1 "+5V" H 2015 5093 50  0000 C CNN
F 2 "" H 2000 4920 50  0001 C CNN
F 3 "" H 2000 4920 50  0001 C CNN
	1    2000 4920
	1    0    0    -1  
$EndComp
Wire Wire Line
	2000 5000 2000 4920
$Comp
L power:+5V #PWR?
U 1 1 5F07C8F3
P 1995 5885
F 0 "#PWR?" H 1995 5735 50  0001 C CNN
F 1 "+5V" H 2010 6058 50  0000 C CNN
F 2 "" H 1995 5885 50  0001 C CNN
F 3 "" H 1995 5885 50  0001 C CNN
	1    1995 5885
	1    0    0    -1  
$EndComp
Wire Wire Line
	1995 5965 1995 5885
$Comp
L power:+5V #PWR?
U 1 1 5F07CE39
P 2000 6835
F 0 "#PWR?" H 2000 6685 50  0001 C CNN
F 1 "+5V" H 2015 7008 50  0000 C CNN
F 2 "" H 2000 6835 50  0001 C CNN
F 3 "" H 2000 6835 50  0001 C CNN
	1    2000 6835
	1    0    0    -1  
$EndComp
Wire Wire Line
	2000 6915 2000 6835
Wire Wire Line
	2300 5100 2200 5100
$Comp
L Device:R R?
U 1 1 5F086C80
P 2445 6065
F 0 "R?" V 2340 5990 50  0000 C CNN
F 1 "1k" V 2340 6135 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 2375 6065 50  0001 C CNN
F 3 "~" H 2445 6065 50  0001 C CNN
	1    2445 6065
	0    1    1    0   
$EndComp
Wire Wire Line
	2295 6065 2195 6065
$Comp
L Device:R R?
U 1 1 5F087429
P 2450 7015
F 0 "R?" V 2345 6940 50  0000 C CNN
F 1 "1k" V 2345 7085 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 2380 7015 50  0001 C CNN
F 3 "~" H 2450 7015 50  0001 C CNN
	1    2450 7015
	0    1    1    0   
$EndComp
Wire Wire Line
	2300 7015 2200 7015
Wire Wire Line
	1700 5400 1800 5400
Text Label 1700 5400 2    50   ~ 0
EN_Prog
Wire Wire Line
	1695 6365 1795 6365
Text Label 1695 6365 2    50   ~ 0
EN_Prog
Wire Wire Line
	1700 7315 1800 7315
Text Label 1700 7315 2    50   ~ 0
EN_Prog
Text Label 2700 5100 0    50   ~ 0
Prog_MOSI
Wire Wire Line
	2600 5100 2700 5100
Text Label 2695 6065 0    50   ~ 0
Prog_SCK
Wire Wire Line
	2595 6065 2695 6065
Text Label 2700 7015 0    50   ~ 0
Prog_MISO
Wire Wire Line
	2600 7015 2700 7015
Wire Wire Line
	2300 5300 2200 5300
Text Label 2300 5300 0    50   ~ 0
Out_PB0
Wire Wire Line
	2295 6265 2195 6265
Text Label 2295 6265 0    50   ~ 0
Out_PB2
Wire Wire Line
	2300 7215 2200 7215
Text Label 2300 7215 0    50   ~ 0
Out_PB1
Text Label 8180 3320 0    50   ~ 0
Prog_RX
Text Label 8180 3420 0    50   ~ 0
Prog_TX
Wire Wire Line
	8180 3420 8070 3420
Wire Wire Line
	8070 3320 8180 3320
Text Label 8180 1920 0    50   ~ 0
Prog_MOSI
Wire Wire Line
	8070 1920 8180 1920
Text Label 8180 2020 0    50   ~ 0
Prog_MISO
Wire Wire Line
	8070 2020 8180 2020
Wire Wire Line
	8070 2120 8180 2120
Text Label 8180 2120 0    50   ~ 0
Prog_MOSI
$Comp
L Device:Crystal Y?
U 1 1 5F06AF5E
P 10340 1960
F 0 "Y?" H 10340 2228 50  0000 C CNN
F 1 "Crystal" H 10340 2137 50  0000 C CNN
F 2 "Crystal:Crystal_SMD_HC49-SD" H 10340 1960 50  0001 C CNN
F 3 "~" H 10340 1960 50  0001 C CNN
	1    10340 1960
	-1   0    0    1   
$EndComp
$Comp
L Device:C C?
U 1 1 5F06AF64
P 10650 1690
AR Path="/5F06AF64" Ref="C?"  Part="1" 
AR Path="/5DA7086F/5F06AF64" Ref="C?"  Part="1" 
AR Path="/5DE868BC/5F06AF64" Ref="C?"  Part="1" 
AR Path="/5EFE6AE5/5F06AF64" Ref="C?"  Part="1" 
F 0 "C?" H 10828 1736 50  0000 L CNN
F 1 "22pF" H 10828 1645 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 10650 1690 50  0001 C CNN
F 3 "~" H 10650 1690 50  0001 C CNN
	1    10650 1690
	-1   0    0    1   
$EndComp
$Comp
L Device:C C?
U 1 1 5F06AF6A
P 10070 1690
AR Path="/5F06AF6A" Ref="C?"  Part="1" 
AR Path="/5DA7086F/5F06AF6A" Ref="C?"  Part="1" 
AR Path="/5DE868BC/5F06AF6A" Ref="C?"  Part="1" 
AR Path="/5EFE6AE5/5F06AF6A" Ref="C?"  Part="1" 
F 0 "C?" H 10248 1736 50  0000 L CNN
F 1 "22pF" H 10248 1645 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 10070 1690 50  0001 C CNN
F 3 "~" H 10070 1690 50  0001 C CNN
	1    10070 1690
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5F06AF70
P 10650 1540
F 0 "#PWR?" H 10650 1290 50  0001 C CNN
F 1 "GND" H 10655 1367 50  0000 C CNN
F 2 "" H 10650 1540 50  0001 C CNN
F 3 "" H 10650 1540 50  0001 C CNN
	1    10650 1540
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5F06AF76
P 10070 1540
F 0 "#PWR?" H 10070 1290 50  0001 C CNN
F 1 "GND" H 10075 1367 50  0000 C CNN
F 2 "" H 10070 1540 50  0001 C CNN
F 3 "" H 10070 1540 50  0001 C CNN
	1    10070 1540
	-1   0    0    1   
$EndComp
Wire Wire Line
	10070 1840 10070 1960
Wire Wire Line
	10070 1960 10190 1960
Wire Wire Line
	10650 1840 10650 1960
Wire Wire Line
	10650 1960 10490 1960
Wire Wire Line
	10070 1960 10070 2220
Connection ~ 10070 1960
Wire Wire Line
	10650 1960 10650 2320
Connection ~ 10650 1960
Text Notes 11085 2540 2    50   ~ 0
Crystal Oscillator
Wire Wire Line
	8070 2220 10070 2220
Wire Wire Line
	8070 2320 10650 2320
Wire Wire Line
	7470 1275 7570 1275
Wire Wire Line
	7570 1275 7570 1320
Wire Wire Line
	7470 1275 7470 1320
$Comp
L power:GND #PWR?
U 1 1 5F07D4A5
P 7470 4360
F 0 "#PWR?" H 7470 4110 50  0001 C CNN
F 1 "GND" H 7320 4280 50  0000 C CNN
F 2 "" H 7470 4360 50  0001 C CNN
F 3 "" H 7470 4360 50  0001 C CNN
	1    7470 4360
	1    0    0    -1  
$EndComp
Wire Wire Line
	7470 4360 7470 4320
Text Label 9615 4600 2    50   ~ 0
Prog_RX
Text Label 9615 4500 2    50   ~ 0
Prog_TX
Wire Wire Line
	9805 4600 9615 4600
Wire Wire Line
	9805 4500 9615 4500
Text Label 6935 5665 2    50   ~ 0
Out_PB0
Text Label 6935 5765 2    50   ~ 0
Out_PB1
Text Label 6935 5865 2    50   ~ 0
Out_PB2
Wire Wire Line
	6935 5865 7010 5865
Wire Wire Line
	7010 5765 6935 5765
Wire Wire Line
	6935 5665 7010 5665
Text Label 2005 1745 1    50   ~ 0
V_Tiny
Wire Wire Line
	2005 1800 2005 1745
Text Label 6935 5565 2    50   ~ 0
V_Tiny
Wire Wire Line
	6935 5565 7010 5565
$Comp
L Connector_Generic:Conn_01x04 J?
U 1 1 5F09A474
P 5525 5675
F 0 "J?" H 5480 5880 50  0000 L CNN
F 1 "Conn_01x04" H 5605 5576 50  0001 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x04_P2.54mm_Vertical" H 5525 5675 50  0001 C CNN
F 3 "~" H 5525 5675 50  0001 C CNN
	1    5525 5675
	-1   0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x04 J?
U 1 1 5F08A5B1
P 7210 5665
F 0 "J?" H 7165 5870 50  0000 L CNN
F 1 "Conn_01x04" H 7290 5566 50  0001 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x04_P2.54mm_Vertical" H 7210 5665 50  0001 C CNN
F 3 "~" H 7210 5665 50  0001 C CNN
	1    7210 5665
	1    0    0    -1  
$EndComp
Text Label 5825 5575 0    50   ~ 0
uC_PB5_RST_A0
Text Label 5825 5675 0    50   ~ 0
uC_PB3_XTAL1_A3
Text Label 5825 5775 0    50   ~ 0
uC_PB4_XTAL2_A2
$Comp
L power:GND #PWR?
U 1 1 5F0A90BF
P 2005 3060
F 0 "#PWR?" H 2005 2810 50  0001 C CNN
F 1 "GND" H 1855 2980 50  0000 C CNN
F 2 "" H 2005 3060 50  0001 C CNN
F 3 "" H 2005 3060 50  0001 C CNN
	1    2005 3060
	1    0    0    -1  
$EndComp
Wire Wire Line
	2005 3060 2005 3000
$Comp
L power:GND #PWR?
U 1 1 5F0ACD24
P 5825 5925
F 0 "#PWR?" H 5825 5675 50  0001 C CNN
F 1 "GND" H 5675 5845 50  0000 C CNN
F 2 "" H 5825 5925 50  0001 C CNN
F 3 "" H 5825 5925 50  0001 C CNN
	1    5825 5925
	1    0    0    -1  
$EndComp
Wire Wire Line
	5725 5875 5825 5875
Wire Wire Line
	5825 5875 5825 5925
Wire Wire Line
	5725 5775 5825 5775
Wire Wire Line
	5725 5675 5825 5675
Wire Wire Line
	5725 5575 5825 5575
$Comp
L Connector_Generic:Conn_01x04 J?
U 1 1 5F0B52D5
P 5225 5675
F 0 "J?" H 5180 5880 50  0000 L CNN
F 1 "Conn_01x04" H 5305 5576 50  0001 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x04_P2.54mm_Vertical" H 5225 5675 50  0001 C CNN
F 3 "~" H 5225 5675 50  0001 C CNN
	1    5225 5675
	-1   0    0    -1  
$EndComp
Wire Wire Line
	5425 5575 5425 5675
Wire Wire Line
	5425 5875 5425 5775
Wire Wire Line
	5425 5775 5425 5675
Connection ~ 5425 5775
Connection ~ 5425 5675
$Comp
L Connector_Generic:Conn_01x04 J?
U 1 1 5F0C68CD
P 4910 5675
F 0 "J?" H 4865 5880 50  0000 L CNN
F 1 "Conn_01x04" H 4990 5576 50  0001 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x04_P2.54mm_Vertical" H 4910 5675 50  0001 C CNN
F 3 "~" H 4910 5675 50  0001 C CNN
	1    4910 5675
	-1   0    0    -1  
$EndComp
Wire Wire Line
	5110 5575 5110 5675
Wire Wire Line
	5110 5875 5110 5775
Wire Wire Line
	5110 5775 5110 5675
Connection ~ 5110 5775
Connection ~ 5110 5675
$Comp
L Connector_Generic:Conn_01x04 J?
U 1 1 5F0C8428
P 7520 5670
F 0 "J?" H 7475 5875 50  0000 L CNN
F 1 "Conn_01x04" H 7600 5571 50  0001 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x04_P2.54mm_Vertical" H 7520 5670 50  0001 C CNN
F 3 "~" H 7520 5670 50  0001 C CNN
	1    7520 5670
	1    0    0    -1  
$EndComp
Wire Wire Line
	7320 5870 7320 5770
Wire Wire Line
	7320 5570 7320 5670
Wire Wire Line
	7320 5670 7320 5770
Connection ~ 7320 5670
Connection ~ 7320 5770
$Comp
L Connector_Generic:Conn_01x04 J?
U 1 1 5F0CEFDC
P 7835 5680
F 0 "J?" H 7790 5885 50  0000 L CNN
F 1 "Conn_01x04" H 7915 5581 50  0001 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x04_P2.54mm_Vertical" H 7835 5680 50  0001 C CNN
F 3 "~" H 7835 5680 50  0001 C CNN
	1    7835 5680
	1    0    0    -1  
$EndComp
Wire Wire Line
	7635 5880 7635 5780
Wire Wire Line
	7635 5580 7635 5680
Wire Wire Line
	7635 5680 7635 5780
Connection ~ 7635 5680
Connection ~ 7635 5780
Text Label 5425 5450 1    50   ~ 0
V_Tiny
$Comp
L power:GND #PWR?
U 1 1 5F0D0563
P 5110 5965
F 0 "#PWR?" H 5110 5715 50  0001 C CNN
F 1 "GND" H 4960 5885 50  0000 C CNN
F 2 "" H 5110 5965 50  0001 C CNN
F 3 "" H 5110 5965 50  0001 C CNN
	1    5110 5965
	1    0    0    -1  
$EndComp
Text Label 7320 5420 1    50   ~ 0
V_Tiny
Wire Wire Line
	7320 5570 7320 5420
Connection ~ 7320 5570
$Comp
L power:GND #PWR?
U 1 1 5F0D229B
P 7635 5995
F 0 "#PWR?" H 7635 5745 50  0001 C CNN
F 1 "GND" H 7485 5915 50  0000 C CNN
F 2 "" H 7635 5995 50  0001 C CNN
F 3 "" H 7635 5995 50  0001 C CNN
	1    7635 5995
	1    0    0    -1  
$EndComp
Wire Wire Line
	7635 5880 7635 5995
Connection ~ 7635 5880
Wire Wire Line
	5110 5965 5110 5875
Connection ~ 5110 5875
Wire Wire Line
	5425 5575 5425 5450
Connection ~ 5425 5575
$EndSCHEMATC
