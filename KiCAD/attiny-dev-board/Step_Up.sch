EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 4 4
Title "Carrera Sensor Board "
Date "2019-10-29"
Rev "V1"
Comp "Hochschule Coburg"
Comment1 "Ersteller: Moritz Vierneusel"
Comment2 ""
Comment3 "Moritz Vierneusel"
Comment4 "Patrick Jahn"
$EndDescr
Wire Wire Line
	7365 3260 7365 3600
$Comp
L Device:C C16
U 1 1 5D508C5E
P 7365 3110
AR Path="/5F01C70C/5D508C5E" Ref="C16"  Part="1" 
AR Path="/5F309CB9/5D508C5E" Ref="C?"  Part="1" 
F 0 "C?" H 7480 3156 50  0000 L CNN
F 1 "22uF 12V" H 7480 3065 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric" H 7403 2960 50  0001 C CNN
F 3 "~" H 7365 3110 50  0001 C CNN
	1    7365 3110
	1    0    0    -1  
$EndComp
$Comp
L Device:D_Schottky D4
U 1 1 5D5572F8
P 6615 2610
AR Path="/5F01C70C/5D5572F8" Ref="D4"  Part="1" 
AR Path="/5F309CB9/5D5572F8" Ref="D?"  Part="1" 
F 0 "D?" H 6465 2745 50  0000 L CNN
F 1 "SS34" H 6645 2745 50  0000 L CNN
F 2 "Diode_SMD:D_SMA" H 6615 2610 50  0001 C CNN
F 3 "http://www.chinesechip.com/files/2015-06/8b5aa353-e559-4708-bf71-5dbd56a0a342.pdf" H 6615 2610 50  0001 C CNN
	1    6615 2610
	-1   0    0    1   
$EndComp
$Comp
L Device:R R11
U 1 1 5D50F042
P 6860 2970
AR Path="/5F01C70C/5D50F042" Ref="R11"  Part="1" 
AR Path="/5F309CB9/5D50F042" Ref="R?"  Part="1" 
F 0 "R?" H 7005 2930 50  0000 C CNN
F 1 "210k" H 7015 3040 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 6790 2970 50  0001 C CNN
F 3 "~" H 6860 2970 50  0001 C CNN
	1    6860 2970
	-1   0    0    1   
$EndComp
$Comp
L Device:R R10
U 1 1 5D51B592
P 6860 3355
AR Path="/5F01C70C/5D51B592" Ref="R10"  Part="1" 
AR Path="/5F309CB9/5D51B592" Ref="R?"  Part="1" 
F 0 "R?" H 6660 3400 50  0000 L CNN
F 1 "40k2" H 6605 3305 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 6790 3355 50  0001 C CNN
F 3 "~" H 6860 3355 50  0001 C CNN
	1    6860 3355
	1    0    0    -1  
$EndComp
$Comp
L Device:L L1
U 1 1 5D507F09
P 5875 2610
AR Path="/5F01C70C/5D507F09" Ref="L1"  Part="1" 
AR Path="/5F309CB9/5D507F09" Ref="L?"  Part="1" 
F 0 "L?" V 5694 2610 50  0000 C CNN
F 1 "22uH" V 5785 2610 50  0000 C CNN
F 2 "Inductor_SMD:L_7.3x7.3_H3.5" H 5875 2610 50  0001 C CNN
F 3 "~" H 5875 2610 50  0001 C CNN
	1    5875 2610
	0    1    1    0   
$EndComp
$Comp
L Device:C C13
U 1 1 5D4E75DF
P 4960 3295
AR Path="/5F01C70C/5D4E75DF" Ref="C13"  Part="1" 
AR Path="/5F309CB9/5D4E75DF" Ref="C?"  Part="1" 
F 0 "C?" H 5075 3341 50  0000 L CNN
F 1 "22uF" H 5075 3250 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric" H 4998 3145 50  0001 C CNN
F 3 "~" H 4960 3295 50  0001 C CNN
	1    4960 3295
	1    0    0    -1  
$EndComp
Text HLabel 4260 3010 0    50   Input ~ 0
Vin
Text HLabel 7545 2610 2    50   Output ~ 0
Vout_12V
Wire Wire Line
	7545 2610 7365 2610
Wire Wire Line
	5310 3110 5310 2760
Wire Wire Line
	5310 2760 4260 2760
Text HLabel 4260 2760 0    50   Input ~ 0
Enable
$Comp
L OLIMEX_Regulators:MT3608(SOT23-6) U?
U 1 1 5F31038B
P 5820 3110
F 0 "U?" H 5870 3425 50  0000 C CNN
F 1 "MT3608(SOT23-6)" H 5870 3334 50  0000 C CNN
F 2 "" H 5850 3260 20  0001 C CNN
F 3 "" H 5920 3110 60  0000 C CNN
	1    5820 3110
	1    0    0    -1  
$EndComp
Wire Wire Line
	5310 3110 5620 3110
Wire Wire Line
	5725 2610 5445 2610
Wire Wire Line
	5445 2610 5445 3010
Connection ~ 5445 3010
Wire Wire Line
	5445 3010 5620 3010
Wire Wire Line
	6025 2610 6335 2610
Wire Wire Line
	6335 2610 6335 3010
Wire Wire Line
	6335 3010 6120 3010
Wire Wire Line
	6335 2610 6465 2610
Connection ~ 6335 2610
Wire Wire Line
	7365 2610 7365 2960
Connection ~ 7365 2610
Wire Wire Line
	7365 2610 6860 2610
Wire Wire Line
	4960 3600 6460 3600
$Comp
L power:GND #PWR036
U 1 1 5DF25201
P 6460 3750
AR Path="/5F01C70C/5DF25201" Ref="#PWR036"  Part="1" 
AR Path="/5F309CB9/5DF25201" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 6460 3500 50  0001 C CNN
F 1 "GND" H 6465 3577 50  0000 C CNN
F 2 "" H 6460 3750 50  0001 C CNN
F 3 "" H 6460 3750 50  0001 C CNN
	1    6460 3750
	1    0    0    -1  
$EndComp
Connection ~ 6460 3600
Wire Wire Line
	6460 3750 6460 3600
Wire Wire Line
	4960 3600 4960 3445
Wire Wire Line
	4260 3010 4960 3010
Wire Wire Line
	4960 3145 4960 3010
Connection ~ 4960 3010
Wire Wire Line
	4960 3010 5445 3010
Wire Wire Line
	6860 2820 6860 2610
Connection ~ 6860 2610
Wire Wire Line
	6860 2610 6765 2610
Wire Wire Line
	6860 3120 6860 3160
Wire Wire Line
	6860 3505 6860 3600
Wire Wire Line
	6460 3600 6860 3600
Connection ~ 6860 3600
Wire Wire Line
	6860 3600 7365 3600
Wire Wire Line
	6120 3210 6785 3210
Wire Wire Line
	6785 3210 6785 3160
Wire Wire Line
	6785 3160 6860 3160
Connection ~ 6860 3160
Wire Wire Line
	6860 3160 6860 3205
$EndSCHEMATC
